# wallet-btc
BTC wallet for address generation, deposits tracking and ethers withdrawals.

#### Development
```
npm install
node src/app.js

[In case of python env variable error do this]

apt-get install python2.7    
ln -s /usr/bin/python2.7 /usr/bin/python 
```

#### Building
##### Docker image build & run
```
docker build -t 'wallet-btc' . > build.log
docker run wallet-btc
```
##### OR using docker-compose
```
docker-compose up
```

#### Configuration Guide
Set these configurations in `development.js` and `production.js`
<details>
<summary>Example configurations</summary>

```sh
currentBlock: 4460536 #syncing will start from this block
kafka.hosts: ['54.165.245.55:9092']
network.uri_ws: "wss://rinkeby.infura.io/ws/mist",
network.uri_http: "https://rinkeby.infura.io/mist"
```

</details>

#### Limitations
For ERC20 tokens only deposits are supported no withdrawals.