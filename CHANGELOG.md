### 1.4.1

* `X360-865` Cold wallet funds are moved considering txfee
* `walllet db` docker volume attached to store bitcore wallets

### 1.4.0

* `B360-22` Calculation of maximum available balance for withdraw
* `X360-805` Fix: Cold wallet selection for funds movement 
* `X360-856` Estimation of network transaction fee