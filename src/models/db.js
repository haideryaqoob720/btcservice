const config = require('config')
const mariadb = require('mariadb');
const Sequelize = require('sequelize');

const log = require('../helpers/logger').create('DATABASE');

const wallet_symbol = config.get('symbol'); // used to identify and store keys in redis
const dbConfig = config.get('database'); // used to identify and store keys in redis

const createDB = 'CREATE DATABASE IF NOT EXISTS ' + dbConfig.connection.dbName;
const BTCCurrency = `insert into ${dbConfig.connection.dbName}.configuration(name,symbol,decimals,feeAccount,conf) ` +
    `values ('BITCOIN','BTC',8,'','{"listed":true,"deposit":true,"withdraw":true,"gasLimit":21000,"network_fee":"0.001",` +
    `"confirmations":1,"opt_percentage":25,"min_withdrawal":"0.001","min_threshold":"5","opt_threshold":"20",` +
    `"exe_threshold":"1000","daily_withdrawal":"1000","calc_opt_threshold":"20"}')`;

//Creating ORM connection
const sequelize = new Sequelize(
    dbConfig.connection.dbName,
    dbConfig.connection.user,
    dbConfig.connection.password,
    {
        host: dbConfig.connection.host,
        port: dbConfig.connection.port,
        dialect: 'mariadb',
        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
        },
        dialectOptions: {
            timezone: 'Etc/GMT0',
        },
        logging: false
    });

sequelize.addHook('beforeUpdate', function (record, options) {
    record.dataValues.updatedAt = Math.floor(Date.now() / 1000);
});

const configuration = sequelize.define('configuration', {
    name: { type: Sequelize.STRING(60), allowNull: false },
    decimals: { type: Sequelize.INTEGER(2), allowNull: false },
    symbol: { type: Sequelize.STRING(20), allowNull: false, primaryKey: true },
    feeAccount: { type: Sequelize.STRING, allowNull: false },
    conf: { type: Sequelize.JSON, allowNull: false },
}, {
    timestamps: false,
    tableName: 'configuration'
});

const unconfirmedDeposits = sequelize.define('unconfirmedDeposits', {
    txid: { type: Sequelize.STRING(128), allowNull: false, primaryKey: true },
    chain: { type: Sequelize.STRING(20), allowNull: false },
    data: { type: Sequelize.JSON, allowNull: false },
}, {
    timestamps: false,
    tableName: 'unconfirmedDeposits'
});

const keypair = sequelize.define('keypair', {
    address: { type: Sequelize.STRING(42), primaryKey: true },
    symbol: { type: Sequelize.STRING(20), allowNull: false },
    pk: { type: Sequelize.STRING(128), allowNull: true },
}, {
    tableName: 'keypair'
});

const balance = sequelize.define('balance', {
    address: { type: Sequelize.STRING(42), primaryKey: true },
    balance: {
        type: Sequelize.DECIMAL(30, 0), allowNull: true, defaultValue: 0,
        validate: { min: 0 }
    },
    locked: {
        type: Sequelize.DECIMAL(30, 0), allowNull: true, defaultValue: 0,
        validate: { min: 0 }
    },
    symbol: { type: Sequelize.STRING(20), allowNull: false, primaryKey: true },
}, {
    tableName: 'balance'
});

const wallet = sequelize.define('wallet', {
    symbol: { type: Sequelize.STRING(20), allowNull: false },
    address: { type: Sequelize.STRING(44), primaryKey: true },
    type: { type: Sequelize.ENUM('hot', 'cold'), allowNull: false },
    locked: { type: Sequelize.DECIMAL(30, 0), allowNull: true, defaultValue: 0 },
    balance: { type: Sequelize.DECIMAL(30, 0), allowNull: false, defaultValue: 0 },
    target_balance: { type: Sequelize.DECIMAL(30, 0), allowNull: false, defaultValue: 0 },
    conf: { type: Sequelize.JSON, allowNull: true },
    deleted: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false },
}, {
    tableName: 'wallet'
});

const transaction = sequelize.define('transaction', {
    id: {
        type: Sequelize.INTEGER,
        unique: true,
        autoIncrement: true
    },
    uuid: { type: Sequelize.STRING(36), allowNull: true },
    hash: { type: Sequelize.STRING(128), allowNull: false, primaryKey: true },
    type: { type: Sequelize.ENUM('deposit', 'withdraw'), allowNull: false, primaryKey: true },
    cold_txid: { type: Sequelize.STRING(128), allowNull: true, defaultValue: null },
    symbol: { type: Sequelize.STRING(20), allowNull: false },
    to: {
        type: Sequelize.TEXT, allowNull: false,
        to: {
            type: Sequelize.STRING(42),
            references: {
                model: 'pk_addresses',
                key: 'address'
            }
        }
    },
    from: { type: Sequelize.STRING(42), allowNull: false },
    value: { type: Sequelize.DECIMAL(30, 0), defaultValue: 0, allowNull: false },
    status: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true },

    txFee: { type: Sequelize.DECIMAL(30, 0), allowNull: true, defaultValue: 0 },
}, { tableName: 'transaction' });


function sync_db() {
    return new Promise(async (resolve, reject) => {
        try {
            await sequelize.sync();
            log.info(`[DATABASE] ${dbConfig.connection.dbName} connected`);
            return resolve();
        } catch (error) {
            log.warn(error.message);
            if (error.message.indexOf('Unknown database') > -1) {
                log.info(`Creating database ${dbConfig.connection.dbName}`);
                create_DB().then(resolve).catch(reject);
            } else {
                const errorMsg = `[sync_db] Unable to connect to database termintaing process ${JSON.stringify(error)}`;
                reject(errorMsg);
            }
        }
    })
}

// creating database if not esists
function create_DB() {
    return new Promise(async (resovle, reject) => {
        try {
            const conn = await mariadb.createConnection(
                {
                    host: dbConfig.connection.host,
                    port: dbConfig.connection.port,
                    user: dbConfig.connection.user,
                    password: dbConfig.connection.password
                })
            const result = await conn.query(createDB);

            await sync_db();

            log.info(`creating currency ${wallet_symbol} with default configurations`);
            await conn.query(BTCCurrency);

            conn.close();

            resovle();

            if (result.affectedRows == 1)
                log.info(`Database Created ${dbConfig.connection.dbName}`)

        } catch (error) {
            log.fatal(`[create_DB] Unable to connect to database server ${error.message}`);
            reject(error);
        }
    })
};

module.exports = {
    sync_db: () => {
        return new Promise((resolve, reject) => {
            sync_db().then(resolve).catch(reject)
        })
    },
    sequelize: () => {
        return sequelize;
    },
    closeConnection: async () => {
        log.info(`[DATABASE] ${dbConfig.connection.dbName} closed`)
        sequelize.close();
    },

    models: () => {
        return {
            configuration, keypair, balance, wallet, transaction, unconfirmedDeposits
        }
    },

    findAll: (model, filter, options = {}) => {
        return new Promise((resolve, reject) => {
            model.findAll(
                Object.assign(options, {
                    raw: true,
                    where: filter
                })
            ).then(resolve)
                .catch(reject);
        })
    },

    findOne: (model, filter, options = {}) => {
        return new Promise((resolve, reject) => {
            model.findOne(
                Object.assign(options, {
                    raw: true,
                    where: filter
                })
            ).then(resolve)
                .catch(reject);
        })
    },

    create: (model, data) => {
        return new Promise((resolve, reject) => {
            log.info(`inserting into ${model.name}`)
            log.debug(`inserting data ${JSON.stringify(data)} into ${model.name}`)
            model.create(data).then(resolve).catch(reject)
        })
    },

    bulkCreate: (model, data, options = {}) => {
        return new Promise((resolve, reject) => {
            model.bulkCreate(data, options).then(resolve).catch(reject)
        })
    },

    bulkUpdate: (model, data, options = {}) => {
        return new Promise((resolve, reject) => {
            model.bulkCreate(data, options).then(resolve).catch(reject)
        })
    },

    update: (model, data, filter) => {
        return new Promise((resolve, reject) => {
            model.update(data, { where: filter }).then(resolve).catch(reject)
        })
    },

    upsert: (model, data) => {
        return new Promise((resolve, reject) => {
            log.info(`Upsert into ${model.name}`)
            log.debug(`Upsert data ${JSON.stringify(data)} into ${model.name}`)
            model.upsert(data).then(resolve).catch(reject)
        })
    },

    destroy: (model, filter) => {
        return new Promise((resolve, reject) => {
            log.debug(`removing records from ${model.name} against ${JSON.stringify(filter)}`)
            model.destroy({ where: filter }).then(resolve).catch(reject)
        })
    },

    findOrCreate: (model, data) => {
        return new Promise((resolve, reject) => {
            model.findOne(
                {
                    where: filter
                },
                {
                    raw: true
                }
            ).then((result) => {
                if (!result) {
                    _create(model, data).then(resolve).catch(reject)
                } else
                    return reject("record already exists")
            })
                .catch(reject);
        })
    }
}