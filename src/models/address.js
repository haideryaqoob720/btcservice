// const config = require('config')
// const bitcoin = require('bitcoinjs-lib')

// const db = require('../models/db')
// const dbModelKeyPair = db.models().keypair
// const log = require('../helpers/logger').create('AddressGen')

// const network = bitcoin.networks[config.network.type]

// module.exports = {
//     // generate basic bitcoin address
//     generateAddress: (symbol) => {
//         return new Promise(async (resolve, reject) => {
//             const keyPair = bitcoin.ECPair.makeRandom({
//                 network: network
//             })
//             const address = bitcoin.payments.p2pkh({
//                 network: network,
//                 pubkey: keyPair.publicKey
//             }).address

//             const dbObj = {
//                 symbol: symbol.toUpperCase(),
//                 address: address,
//                 pk: keyPair.toWIF() //getting private key
//             }

//             await db.create(dbModelKeyPair, dbObj)

//             resolve(address)
//         })
//     }
// }