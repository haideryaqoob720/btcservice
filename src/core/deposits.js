const config = require('config')
const redis = require('../helpers/redis')
const coldStorage = require('./coldStorage')
const helper = require('../helpers/helper')
const log = require('../helpers/logger').create("DEPOSITS")

const { transactionConfirmed, exHotWallet } = config.kafka.producer.topics
const btcDeposit = `btc.${transactionConfirmed}`

module.exports = {

    BTC: {
        deposit: async (data) => {

            try {
                // converting value into human readable formate
                data.value = data.value / 1e8

                const walletType = await helper.walletType(data.chain, data.address)

                log.info({ walletType })

                log.info(`Verifying deposit tx: ${data.mintTxid} is new or already processed`)
                const isAlreadyProcessed = redis.get('existsAsync', `${data.chain.toLowerCase()}:transaction:${data.mintTxid}`)

                if (!isAlreadyProcessed == 1) {
                    log.warn(`Deposit tx: ${data.mintTxid} already processed`)
                    return;
                }

                switch (walletType) {

                    case 'user': {
                        log.info(`[UserHotWallet] ${data.value} ${data.chain} deposit on ${data.address}`)

                        notifyOnUserHotWalletDeposit(data)

                        await hotWalletBalancer(data.chain)
                    }

                    case 'cold': {
                        break
                    }

                    case 'change': {
                        log.info(`${data.chain} ${data.value} received on change address ${data.address}`)
                        break
                    }

                    case 'hot': {
                        log.info(`[ExHotWallet] ${data.value} ${data.chain} deposit on ${data.address}`)
                        notifyOnExchangeHotWalletDeposit(data)
                        return await coldStorage.isColdWalletWithdraw(data)
                    }

                    default: {
                        log.warn(`${data.chain} unkown deposit found on ${data.address}`)
                        return;
                    }
                }
            } catch (error) {
                log.error(error)
            }
        }
    }
}

function notifyOnUserHotWalletDeposit(data) {
    helper.broadcastMessage(btcDeposit, {
        from: data.chain,
        symbol: data.chain,
        type: 'deposit',
        chain: data.chain,
        tx_id: data.mintTxid,
        value: data.value,
        to: data.address,
        timestamp: Math.floor((Date.now()) / 1000),
    })
}

function notifyOnExchangeHotWalletDeposit(data) {
    helper.broadcastMessage(exHotWallet, {
        method: 'exhot.deposit',
        data: {
            to: data.address,
            uuid: data.mintTxid.substring(0, 36),
            from: data.chain,
            type: 'deposit',
            hash: data.mintTxid,
            chain: data.chain,
            symbol: data.chain,
            status: true,
            value: data.value
        }
    })
}

function hotWalletBalancer(currency) {
    return new Promise(async (resolve, reject) => {
        coldStorage[currency](currency).then(resolve).catch(reject)
    })
}
