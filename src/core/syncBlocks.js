const config = require('config')
const _ = require('lodash')

const db = require('../models/db')
const redis = require('../helpers/redis')
const helper = require('../helpers/helper')

const log = require('../helpers/logger').create("SYNC_PRVS_BLK")

const btcConf = config.chains.bitcoin
module.exports = {
    init: {
        BTC: () => {
            return new Promise(async (resolve, reject) => {
                try {
                    const addresses = await helper.getAddresses.BTC()

                    const blockTipUrl = `${btcConf.baseUrl}/api/BTC/${btcConf.network_type}/block/tip`
                    const latestBlock = (await helper.httpRequest({
                        method: 'GET',
                        uri: blockTipUrl,
                    })).height;

                    let last_block_number = await blockToStart('btc') || latestBlock;
                    const blocksToSync = latestBlock - parseInt(last_block_number)
                    log.info({ latestBlock, last_block_number, blocksToSync })

                    if (blocksToSync == 0){
                        resolve()
                        return await treverseNextBlock(latestBlock + 1, addresses)
                    }

                    if (blocksToSync < 1)
                        return;

                    const blocksDataUrl = `${btcConf.baseUrl}/api/BTC/${btcConf.network_type}/block`
                    let blocks = await helper.httpRequest({
                        method: 'GET',
                        uri: blocksDataUrl,
                        qs: {
                            limit: blocksToSync
                        }
                    })

                    blocks = blocks.reverse()

                    resolve()
                    await processBlocks(blocks, addresses)

                } catch (error) {
                    reject(error)
                }
            })
        }
    }
}

function addDepositIntoQueue(event) {
    return new Promise(((resolve, reject) => {
        log.info(`Adding tx:${event.mintTxid} into pending deposits`)
        db.create(db.models().unconfirmedDeposits,
            {
                txid: event.mintTxid,
                chain: event.chain,
                data: JSON.stringify(event)
            }
        )
            .then(resolve)
            .catch(error => {
                log.error(error)
                resolve()
            })
    }))
}

function treverseNextBlock(blockNumber, addresses, timeout) {
    return new Promise(async (resolve, reject) => {
        console.log({ blockNumber, timeout })
        const _timeout = timeout || 60 * 1000
        try {
            log.info(`Treversing block: ${blockNumber}`)
            const blockDataUrl = `${btcConf.baseUrl}/api/BTC/${btcConf.network_type}/block/${blockNumber}`
            let blocks = await helper.httpRequest({
                method: 'GET',
                uri: blockDataUrl
            })
            // console.log(blocks)
            await processBlocks(new Array(blocks), addresses)
            log.info(`Treversing block: ${blockNumber} Done!`)
            return resolve()
        } catch (error) {
            log.error(error.message)
            if (error.statusCode == 404)
                setTimeout(treverseNextBlock, _timeout, blockNumber, addresses, _timeout);
            else
                return
        }
    })
}

function processBlocks(blocks, addresses) {
    return new Promise(async (resolve, reject) => {
        try {
            for await (let block of blocks) {
                // storing last sync block into redis
                await redis.set('setAsync', `${block.chain.toLowerCase()}:last-block`, block.height)

                log.info(`Current Syncing: ${block.height}`)
                const blockTxUrl = `${btcConf.baseUrl}/api/${block.chain}/${btcConf.network_type}/block/${block.hash}/coins/500/1`
                const transactions = await helper.httpRequest({
                    method: 'GET',
                    uri: blockTxUrl
                })

                transactions.outputs.map(async (tx, index) => {
                    if (_.indexOf(addresses, tx.address) > -1) {

                        // log.info(`Verifying deposit tx: ${tx.mintTxid} is new or already processed`)
                        const isAlreadyProcessed = await redis.get('existsAsync', `btc:transaction:${tx.mintTxid}`)
                        if (isAlreadyProcessed == 0) {
                            await addDepositIntoQueue(tx)

                            await redis.set('setAsync', `${tx.chain.toLowerCase()}:transaction:${tx.mintTxid}`, '{}')
                            log.info(`Tx: ${tx.mintTxid} written into redis`)
                        }
                    }
                })
            }
            resolve()
        } catch (error) {
            reject(error)
        }
    })
}

function blockToStart(chain) {
    return new Promise(async (resolve) => {
        let last_block_number = await redis.get('getAsync', `${chain.toLowerCase()}:last-block`);

        if (last_block_number)
            last_block_number = last_block_number;
        else if (config.get('currentBlock') != 'latest')
            last_block_number = config.get('currentBlock');
        else
            null

        return resolve(last_block_number)
    })
}