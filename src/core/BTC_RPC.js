const fs = require('fs')
const config = require('config')
const rp = require('request-promise')
const io = require('socket.io-client')
const bitcore = require('bitcore-lib')
const AsyncLock = require('async-lock');
const bitcore_client = require("bitcore-client");

const db = require('../models/db')
const deposits = require('./deposits')
const redis = require('../helpers/redis')
const syncBlocks = require('./syncBlocks')
const log = require('../helpers/logger').create("BTC_RPC")
const _conf = config.chains.bitcoin
const network = _conf.network_type
const path = _conf.level_db_wallet_path
const { confirmations } = config
const baseUrl = _conf.baseUrl
const lock = new AsyncLock();
const chain = 'BTC'
let wallet;
let blocksSocket = false;
let walletSocket = false;

module.exports = {

    init: (startFromLastStoredBlock = true) => {
        return new Promise(async (resolve, reject) => {
            try {
                const wallets_path = await loadWallets(path)

                wallet = await bitcore_client.Wallet.loadWallet({ name: _conf.wallet_name, path: wallets_path, createIfMissing: true })

                await wallet.unlock('');
                // log.info('wallet: ', wallet.authPubKey)

                const balance = await wallet.getBalance()
                log.info(balance)

                // socket authentication
                const pubKey = wallet.authPubKey
                const payload = { method: 'socket', url: `${baseUrl}/api` };

                const authClient = new bitcore_client.Client({ apiUrl: `${baseUrl}/api`, authKey: wallet.client.authKey });

                const authPayload = { pubKey, message: authClient.getMessage(payload), signature: authClient.sign(payload) };

                const depositEnabled = await isDepositEnabled();
                log.info(`Is deposit of ${chain} enabled ${depositEnabled}`);
                if (!depositEnabled) {
                    log.error('syncing not started deposit is disabled');
                } else {
                    await initBlockSocket(baseUrl)
                    await initWalletSocket(baseUrl, authPayload)
                    if (startFromLastStoredBlock) {
                        await syncBlocks.init.BTC()
                    }
                }
                resolve()

            } catch (error) {
                log.error(error)
                if (error.statusCode = 404) {
                    log.info(`Creating wallet...`)
                    await createWallet()
                    return await module.exports.init()
                }
                reject(error)
            }
        })
    },

    close: () => {
        if (wallet)
            wallet.lock();
        if (blocksSocket)
            blocksSocket.close();
        if (walletSocket)
            walletSocket.close();
        blocksSocket = null;
        walletSocket = null;
        return;
    },

    generateAddress: () => {
        return new Promise((resolve, reject) => {
            wallet.nextAddressPair()
                .then(nextAddressPair => {
                    return resolve(nextAddressPair[0])
                })
                .catch(reject)
        })
    },

    getUtxos: () => {
        return new Promise(async (resolve, reject) => {

            const utxos = new Array();
            wallet.getUtxos({ includeSpent: false })
                .on('data', data => {
                    const stringData = data.toString().replace(',\n', '');
                    if (stringData.includes('{') && stringData.includes('}')) {
                        utxos.push(JSON.parse(stringData));
                    }
                })
                .on('complete', () => {
                    log.info(`Total utxos `, (utxos || []).length);
                    resolve(utxos);
                })
                .on('error', reject)
        })
    },

    newTx: (params) => {
        return new Promise(async (resolve, reject) => {
            wallet.newTx(params).then(resolve).catch(reject)
        })
    },

    signTx: (params) => {
        return new Promise(async (resolve, reject) => {
            try {
                log.info(`signing transaction utxos length: ${params.utxos.length} `)
                const keys = await privateKeys(params.signingAddresses)
                const privKeys = keys.map(key => key.privKey)

                let bitcore_transaction = new bitcore.Transaction()
                    .from(params.utxos)
                    .to(params.recipient, params.amount)
                    .change(params.change)
                    .fee(params.fee)
                    .sign(privKeys);

                resolve(bitcore_transaction.toString())
                // wallet.signTx(params).then(resolve).catch(err => console.log(err))
            } catch (error) {
                reject(error)
            }
        })
    },

    broadcast: (params) => {
        return new Promise(async (resolve, reject) => {
            wallet.broadcast(params).then(resolve).catch(reject)
        })
    },

    listTransactions: () => {
        return new Promise(async (resolve, reject) => {
            const txs = new Array();
            wallet.listTransactions()
                .on('data', data => {
                    const stringData = data.toString().replace(',\n', '');
                    if (stringData.includes('{') && stringData.includes('}')) {
                        txs.push(JSON.parse(stringData));
                    }
                })
                .on('complete', () => {
                    log.info(`Listed `, (txs || []).length);
                    resolve(txs);
                })
                .on('error', reject)
        })
    },

    getNetworkFee: () => {
        return new Promise(async (resolve, reject) => {
            wallet.getNetworkFee()
                .then(networkFee => {
                    return resolve(networkFee)
                })
                .catch(reject)
        })
    },

    getBalance: () => {
        return new Promise(async (resolve, reject) => {
            wallet.getBalance()
                .then(balance => {
                    return resolve(balance)
                })
                .catch(reject)
        })
    },

    getBlock: (hash) => {
        return new Promise(async (resolve, reject) => {
            // wallet.getBlock()
            //     .then(block => {
            //         return resolve(block)
            //     })
            //     .catch(reject)
        })
    },

    getAddresses: () => {
        return new Promise(async (resolve, reject) => {
            wallet.getAddresses()
                .then(addresses => {
                    return resolve(addresses)
                })
                .catch(reject)
        })
    },

    addressBalance: () => {
        return new Promise(async (resolve, reject) => {
            // wallet.nextAddressPair()
            //     .then(nextAddressPair => {
            //         return resolve(nextAddressPair[0])
            //     })
            //     .catch(reject)
        })
    }
}

async function loadWallets(path) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, { recursive: true })
        bitcore_client.Storage({ path, createIfMissing: true })
        await createWallet()
    }
    return path
}

function createWallet() {
    return new Promise((resolve, reject) => {
        bitcore_client.Wallet.create({
            chain: chain,
            password: '',
            network: network,
            path: path,
            name: _conf.wallet_name,
            baseUrl: `${baseUrl}/api`,
            phrase: _conf.wallet_seed,
        }).then(result => {
            log.info(`Wallet: ${result.name} created successfully!`)
            resolve(result)
        }).catch(reject)
    })
}


function privateKeys(addresses) {
    return new Promise(async (resolve, reject) => {
        try {
            const keys = await wallet.storage.getKeys({
                addresses: addresses,
                name: _conf.wallet_name,
                encryptionKey: wallet.unlocked.encryptionKey
            });
            resolve(keys)
        } catch (error) {
            reject(error)
        }
    })
}

function initWalletSocket(baseUrl, authPayload) {
    return new Promise((resolve, reject) => {
        try {

            if (walletSocket)
                return;

            const socket = io.connect(baseUrl, { transports: ['websocket'] })
            socket.on('connect', (res) => {

                walletSocket = socket;

                log.info(`${chain} Wallet Socket Connected`);

                socket.emit('room', `/${chain}/${network}/wallet`, authPayload);

                socket.on('coin', async (event) => {
                    log.info(`Coin event received @ ${event.coin.address}`);
                    log.debug(event)

                    // adding deposit into queue for confirmations
                    await addDepositIntoQueue(event.coin)

                    await redis.set('setAsync', `${event.coin.chain.toLowerCase()}:transaction:${event.coin.mintTxid}`, '{}')
                    log.info(`Tx: ${event.coin.mintTxid} written into redis`)
                });

                socket.on('tx', (event) => {
                    log.info(`Transaction event received ${event.tx.txid}`);
                    log.debug(event)
                });

                socket.on('failure', (err) => {
                    log.error(`${chain} Socket Connection Error: `, err);
                });

                socket.on('disconnect', (err) => {
                    log.error(`${chain} Socket disconnected: `, err);
                });

                resolve(socket)
            });

            socket.on('error', (err) => {
                log.error(`${chain} Socket Connection Error: `, err);
                reject(err)
            });
        } catch (error) {
            log.error(error)
        }
    })
}

function initBlockSocket(baseUrl) {
    return new Promise((resolve, reject) => {

        try {

            if (blocksSocket)
                return;

            const socket = io.connect(baseUrl, { transports: ['websocket'] })
            socket.on('connect', (res) => {

                blocksSocket = socket;

                log.info(`${chain} Block Socket Connected`);

                socket.emit('room', `/${chain}/${network}/inv`);

                socket.on('block', async (event) => {
                    log.info('New Block: ', event.height);
                    log.debug(event)

                    // storing last sync block into redis
                    await redis.set('setAsync', `${event.chain.toLowerCase()}:last-block`, event.height)

                    //verifying deposit confirmations
                    await broadcastConfirmedDeposits()
                });

                socket.on('failure', (err) => {
                    log.error(`${chain} Socket Connection Error: `, err);
                });

                socket.on('disconnect', (err) => {
                    log.error(`${chain} Socket disconnected: `, err);
                });

                resolve(socket)
            });

            socket.on('error', (err) => {
                log.error(`${chain} Socket Connection Error: `, err);
                reject(err)
            });

        } catch (error) {
            log.error(error)
        }
    })
}

function isDepositEnabled() {
    return new Promise(async (resolve) => {
        const result = await db.models().configuration.findOne();
        resolve(result ? result.conf.deposit : false);
    });
}

function addDepositIntoQueue(event) {
    return new Promise(((resolve, reject) => {
        log.info(`Adding tx:${event.mintTxid} into pending deposits`)
        db.create(db.models().unconfirmedDeposits,
            {
                txid: event.mintTxid,
                chain: event.chain,
                data: JSON.stringify(event)
            }
        )
            .then(resolve)
            .catch(error => {
                log.error(error)
                resolve()
            })
    }))
}

function broadcastConfirmedDeposits() {
    return new Promise(async (resolve, reject) => {
        try {

            lock.acquire(resolve, async function () {

                const pendingDeposits = await db.findAll(
                    db.models().unconfirmedDeposits,
                    {
                        chain: 'BTC'
                    }
                )

                log.info(`Pending depoists confirmations queue: ${pendingDeposits.length}`)

                for await (let deposit of pendingDeposits) {

                    const url = `${baseUrl}/api/${chain}/${network}/tx/${deposit.txid}`
                    const response = await rp({
                        method: 'GET',
                        uri: url,
                        json: true
                    })
                    if (response.confirmations >= confirmations) {

                        const event = JSON.parse(deposit.data)

                        deposits.BTC.deposit(event)

                        log.info(`Removing tx:${deposit.txid} from pending deposits`)
                        await db.destroy(db.models().unconfirmedDeposits, {
                            txid: deposit.txid
                        })

                    } else
                        log.info(`${deposit.txid}, confirmations:${response.confirmations}`)
                }
            }).then(function (current_nonce) {
                // lock released
                return resolve();
            });
        } catch (error) {
            reject(error)
        }
    })
}