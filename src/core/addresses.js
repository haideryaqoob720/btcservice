const config = require('config')

const btc = require('./BTC_RPC')
const log = require('../helpers/logger').create("DEPOSITS")

const newDeposit = config.kafka.producer.topics.transactionConfirmed

module.exports = {

    BTC: () => {
        return new Promise(async (resolve, reject) => {
            btc.generateAddress().then(resolve).catch(reject)
        })
    }
}
