const config = require('config')
var AsyncLock = require('async-lock');
const bigNumber = require('bignumber.js')

const db = require('../models/db')
const redis = require('../helpers/redis')
const helper = require('../helpers/helper');
const { reject } = require('bluebird');
const log = require('../helpers/logger').create("WITHDRAWALS")

const { expenses } = config.kafka.producer.topics
const utxosConfirmations = config.confirmations;
const estimateFeeForTxConfirm = config.confirmations;

const btcDefaultFee = 0.0001 * 1E8 //stoshi
let lock = new AsyncLock();

module.exports = {

    BTC: (data) => {
        return new Promise(async (resolve, reject) => {
            lock.acquire(data.uuid, async function () {
                try {

                    const withdrawalEnabled = await isWithdrawalEnabled(data.currency);
                    log.info(`Is withdrawal of ${data.currency} enabled ${withdrawalEnabled}`);
                    if (!withdrawalEnabled)
                        return reject('withdrawal is disabled');

                    log.info(`Withdraw request`, data.user_id ? `of User ${data.user_id}` : '', `Moving ${data.value} SATOSHIS to ${data.withdrawalAddress}`)
                    let walletBalance = (await helper.getWalletBalance.BTC()).confirmed
                    walletBalance = new bigNumber(walletBalance).minus(btcDefaultFee) // wallet balance is balance - fee
                    if (!walletBalance.gt(data.value))
                        throw ('wallet is out of balance')

                    log.info(`Processing withdrawal of BTC ${data.value}`, data.uuid ? ` uuid: ${data.uuid}` : ' hot to cold')

                    const amount = data.value.toNumber()
                    const recipient = data.withdrawalAddress
                    const amountWithFee = data.value.plus(btcDefaultFee)

                    const [{ utxos, utxosSign }, change] = await Promise.all([
                        getUtxos(amountWithFee),
                        helper.generateAddress['BTC']()
                    ])

                    if (utxos.length == 0) {
                        throw (new Error('no confimred unspent output found'))
                    }

                    const signingAddresses = utxos.map(utxo => utxo.address)
                    const fee = await estimateFee('BTC', utxos.length, recipients_length = 1)
                    await redis.set('setAsync', `btc:address:change:${change}`, change)

                    let params = { recipient, amount, utxos, change, fee, signingAddresses };
                    log.debug(params)
                    // console.log({ utxosSign })
                    const signedTx = await helper.signTx.BTC(params)

                    const transaction = await helper.broadcastSignedTx.BTC({ tx: signedTx })
                    log.info({ transaction })

                    const resp = {
                        hash: transaction.txid,
                        fee: fee,
                    }

                    // broadcasting expenses
                    helper.broadcastMessage(expenses, {
                        chain: 'BTC',
                        currency: 'BTC',
                        uuid: data.uuid || transaction.txid.substring(0, 36),
                        description: data.uuid ? 'withdrawal fee' : 'cold deposit fee',
                        expenses: helper.convertFromBase('BTC', fee)
                    })

                    resolve(resp)
                } catch (error) {
                    reject(error)
                }
            }).then(function (lock) {
                // lock released
                log.info(`lock release`)
                // return resolve({ current_nonce });
            }).catch(function (error) {
                log.error(error)
                // lock released
                log.info(`lock release on error`)
                // return resolve({ current_nonce });
            });
        })
    },

    estimatedFee: {
        BTC: (amount) => {
            return new Promise(async (resolve) => {
                if (!amount)
                    log.info(`Calulating fee against total confirmed wallet balance`);

                let walletBalance = amount || (await helper.getWalletBalance.BTC()).confirmed;
                const { utxos } = await getUtxos(walletBalance);
                const fee = await estimateFee('BTC', utxos.length, recipients_length = 1)
                resolve({ amount: amount || walletBalance, fee })
            })
        }
    }
}

function getUtxos(amount) {
    return new Promise(async (resolve, reject) => {
        try {
            log.info(`Getting utxos of amount: ${amount}`)
            let allUtxos = await helper.getUtxos.BTC()

            let amountLeft = amount
            let utxos = []
            let utxosSign = []
            allUtxos.every(function (utxo, index) {
                if (amountLeft <= 0)
                    return false

                if (utxo.confirmations < utxosConfirmations)
                    return true;

                amountLeft -= utxo.value
                utxos.push({
                    outputIndex: utxo.mintIndex,
                    txId: utxo.mintTxid,
                    address: utxo.address,
                    script: utxo.script,
                    satoshis: utxo.value,
                })
                utxosSign.push(utxo)
                return true;
            })
            const sumUtxos = utxos
                .map(item => item.satoshis)
                .reduce((prev, curr) => prev + curr, 0);
            log.info(`selected utxos: ${utxos.length} sum of utxos: `, sumUtxos)
            return resolve({ utxos, utxosSign })
        } catch (error) {
            reject(error)
        }
    })
}

function estimateFee(chain, utxos_length, recipients_length) {
    return new Promise(async (resolve, reject) => {
        switch (chain) {
            case 'BTC': {
                const utxoBytes = 220;
                const outputBytes = 34;
                const transactionHeader = 10;
                const transactionSize = (((utxos_length * utxoBytes) + (recipients_length * outputBytes)) + transactionHeader + utxos_length);
                helper.getNetworkFee.BTC(estimateFeeForTxConfirm).then(calculatedNetworkFeeKb => {
                    const netWorkFeeSat = (JSON.parse(calculatedNetworkFeeKb).feerate || 0.001) * 1E8 / 1000;
                    const totalTransactionFee = parseInt(transactionSize * netWorkFeeSat);
                    log.info(`Fee: utxos: ${utxos_length} totalFee: ${totalTransactionFee}`)
                    resolve(totalTransactionFee);
                }).catch(error => {
                    log.error(error)
                    resolve(btcDefaultFee)
                })
                break;
            }
            default: {
                reject(new Error('unkown chain'))
            }
        }
    })
}

function isWithdrawalEnabled() {
    return new Promise(async (resolve) => {
        const result = await db.models().configuration.findOne();
        resolve(result ? result.conf.withdraw : false);
    });
}