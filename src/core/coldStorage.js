const _ = require('lodash')
const sequelize = require('sequelize')
const bigNumber = require('bignumber.js')

const db = require('../models/db')
const redis = require('../helpers/redis')
const helper = require('../helpers/helper')
const withdrawals = require('./withdrawals')
const log = require('../helpers/logger').create("COLD_STORAGE")

const btcConf = config.chains.bitcoin
const { coldWallets } = config.kafka.producer.topics

module.exports = {
    BTC: async (currency) => {
        return new Promise(async (resolve, reject) => {

            try {
                const value = await amount_move_to_cold_storage(currency)
                if (value < 1)
                    return;

                const coldWalletAddress = await helper.coldWalletAddress(value, currency)

                log.info(`[${currency} CW] MOVING ${value} to ${coldWalletAddress}`);

                const { fee } = await withdrawals.estimatedFee[currency](value);

                const amountToWithdraw = new bigNumber(value).minus(fee);

                const withdrawalTx = await withdrawals[currency]({
                    value: amountToWithdraw,
                    withdrawalAddress: coldWalletAddress
                })

                // writing internal tx for txFee as withdrawal fee
                const data = {
                    from: 'BTC',
                    status: true,
                    amount: helper.convertFromBase(currency, amountToWithdraw),
                    type: 'deposit',
                    chain: currency,
                    symbol: currency,
                    fee: withdrawalTx.fee,
                    to: coldWalletAddress,
                    hash: withdrawalTx.hash,
                    uuid: withdrawalTx.hash.substring(0, 36),
                    description: 'Funds moved to cold wallet',
                }

                // updating cold wallet balance
                await addColdWalletBalance(coldWalletAddress, currency, amountToWithdraw)

                // await helper.writeInternalTx(internalTx)

                // broadcasting event to update cold wallet balances
                data.fee = helper.convertFromBase('BTC', data.fee)
                helper.broadcastMessage(coldWallets, {
                    method: 'deposit',
                    data: data
                })

                return resolve(withdrawalTx)
            } catch (error) {
                log.error(error)
                reject(error);
            }
        })
    },

    isColdWalletWithdraw: (data) => {
        return new Promise(async (resolve, reject) => {
            try {
                log.info(`Verifying if its a withdrawal from cold wallet to Ex Hot wallet`)
                const blockTxUrl = `${btcConf.baseUrl}/api/${data.chain}/${btcConf.network_type}/tx/${data.mintTxid}/coins`
                const feeTxUrl = `${btcConf.baseUrl}/api/${data.chain}/${btcConf.network_type}/tx/${data.mintTxid}`
                const [utxos, transaction, coldwallets] = await Promise.all([
                    helper.httpRequest({
                        method: 'GET',
                        uri: blockTxUrl
                    }),
                    helper.httpRequest({
                        method: 'GET',
                        uri: feeTxUrl
                    }),
                    redis.get('keysAsync', `${data.chain.toLowerCase()}:address:cold*`)
                ])

                if (coldwallets.toString().length == 0) {
                    log.warn(`${data.chain}  No cold wallet added found`)
                    return resolve();
                }

                const inputAddresses = utxos.inputs.map(utxo => { return utxo.address })
                const changeAddressUtxos = utxos.outputs.filter(utxo => {
                    if (utxo.address != data.address)
                        return utxo.address
                })
                const outputAddresses = changeAddressUtxos.map(utxo => { return utxo.address })
                for await (let coldwallet of coldwallets) {
                    const walletDetails = JSON.parse(await redis.get('getAsync', coldwallet))
                    if (_.intersection(walletDetails.addresses, inputAddresses).length > 0) {
                        data.from = walletDetails.addresses[0]
                        walletDetails.addresses = walletDetails.addresses.concat(outputAddresses)
                        // updating addresses against cold wallet
                        redis.set('setAsync', coldwallet, JSON.stringify(walletDetails))
                        break;
                    }
                }
                // coldwallet is found
                if (data.from) {
                    data.fee = transaction.fee;
                    const amount = helper.convertToBase(data.chain, data.value).toNumber() + transaction.fee;
                    await subColdWalletBalance(data.from, data.chain, amount)
                    notifyOnWithdraw(data);
                } else {
                    log.warn(`${data.chain} deposit on exchange how wallet but unable to track sender ` +
                        `${data.mintTxid}`);
                    return resolve();
                }
            } catch (error) {
                log.error(error)
            }
        })
    }
}

function amount_move_to_cold_storage(currency) {
    return new Promise(async (resolve) => {

        let confirmedBalance = (await helper.getWalletBalance[currency]()).confirmed
        confirmedBalance = new bigNumber(confirmedBalance)

        const config = await db.findOne(
            db.models().configuration,
            {
                symbol: currency
            },
            {
                attributes: ['conf', 'decimals']
            }
        )

        const max_threshold = new bigNumber(config.conf.calc_opt_threshold).plus(config.conf.exe_threshold)
        const opt_threshold_base = helper.convertToBase(currency, config.conf.opt_threshold)
        const max_threshold_base = helper.convertToBase(currency, max_threshold)

        const amount_to_move = confirmedBalance.minus(opt_threshold_base)

        if (confirmedBalance.gte(max_threshold_base) && amount_to_move > 0) {
            log.info(`[${currency} CW] MOVING ${amount_to_move} to cold wallets max_threshold: ` +
                `${max_threshold_base} current balance: ${confirmedBalance} op_thres: ${opt_threshold_base}`);
            return resolve(amount_to_move);
        } else {
            log.info(`[${currency} CW] NOT MOVING funds to cold wallets threshold: ${max_threshold_base} ` +
                `current balance: ${confirmedBalance} optimal threshold ${opt_threshold_base}`);
            return resolve(0);
        }
    })
}

function addColdWalletBalance(address, symbol, amount) {
    return new Promise((resolve, reject) => {
        log.info(`Updataing balance of cold wallet ${address} +${amount}`)
        db.update(
            db.models().wallet,
            {
                balance: sequelize.literal(`balance + ${amount}`)
            },
            {
                address,
                symbol,
                type: 'cold'
            }
        )
            .then(resolve).catch(reject)
    })
}

function subColdWalletBalance(address, symbol, amount) {
    return new Promise((resolve, reject) => {
        log.info(`Updataing balance of ${symbol} cold wallet ${address} -${amount}`)
        db.update(
            db.models().wallet,
            {
                balance: sequelize.literal(`balance - ${amount}`)
            },
            {
                address,
                symbol,
                type: 'cold'
            }
        )
            .then(resolve).catch(reject)
    })
}

function notifyOnWithdraw(data) {
    return new Promise(async (resolve, reject) => {
        log.info(`[COLD WALLET] notifying at ${coldWallets}`)
        helper.broadcastMessage(coldWallets, {
            method: 'withdraw',
            data: {
                status: true,
                from: data.from,
                to: data.address,
                type: 'withdraw',
                chain: data.chain,
                symbol: data.chain,
                amount: data.value,
                hash: data.mintTxid,
                uuid: data.mintTxid.substring(0, 36),
                description: 'Funds moved from cold wallet',
                fee: helper.convertFromBase(data.chain, data.fee).toString()
            }
        })
        return resolve()
    })
}