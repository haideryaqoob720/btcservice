// ===========================================
// Load Modules
// ===========================================
const config = require('config');
const graceful = require('node-graceful')

// ===========================================
// Imports
// ===========================================
const logger = require('./helpers/logger');
const helper = require('./helpers/helper');

const wallet_symbol = config.get('symbol');

// ===========================================
// Configurations
// ===========================================
const options = { logFolder: config.get('log.logFolder'), logLevel: config.get('log.level') };
logger.setup(options);
const log = logger.create('main');

//loading default configurations 
helper.init(wallet_symbol).then(conf_loaded => {
    if (conf_loaded)
        log.info(`${wallet_symbol} Default configurations loaded `);
}).catch((error) => {
    log.error(`Unable to load default configuration of ${wallet_symbol}\nTerminating Application`);
    process.exit(1);
});

// ----------------------------------------
// Global application error catch
// ----------------------------------------

process.on("uncaughtException", (err) => {
    log.fatal("uncaughtException: ", err);
})

process.on('unhandledRejection', error => {
    log.fatal('unhandledRejection', error);
});

// Terminate active connection on ctrl+c
process.on('SIGINT', async function () {
    console.log('SIGINT Shutdown received.');
    await helper.closeConnections();
    process.exit(0)
});

// //Terminate active connection on kill
process.on('SIGTERM', async function () {
    console.log('SIGTERM Shutdown received.');
    await helper.closeConnections();
    process.exit(0)
});

// graceful.on('exit', async () => {
//     log.info('SHUTTING DOWN - Terminating active connections.')
//     await helper.closeConnections();
//     process.exit(0)
// })


setTimeout(() => {
    require('./core/deposits')
    // require('./core/bitcoreClient')
    // require('./core/bitcoreSoketIo')
}, 1000);