'use strict'

/*=========================
 * Kafka client works as both consumer and producer
 *=========================*/
const path = require('path')
const Kafka = require('node-rdkafka')

const config = require('config').get('kafka')
const log = require('./logger').create("KAFKA");

const default_options = {
    'client.id': config.name,
    // "security.protocol": "ssl",
    "ssl.key.password": "asdasd",
    'metadata.broker.list': config.hosts,
    "ssl.ca.location": (path.join(__dirname, '../../ssl/ca-cert.pem')),
    "ssl.certificate.location": (path.join(__dirname, '../../ssl/client.pem')),
    "ssl.key.location": (path.join(__dirname, '../../ssl/client.key')),
}

if ((config.util.getEnv('NODE_ENV') == 'development'))
    delete default_options['security.protocol']

console.log("INITIALIZING KAFKA CONSUMER & PRODUCER")

/*========== SETTING UP DEFAULTS FOR KAFKA CONSUMER ==========*/

var consumer;
const options_consumer = {
    'enable.auto.commit': true,
    'group.id': `${config.name}_consumer`
}
function connectConsumer() {
    return new Promise((resolve, reject) => {

        const commandProcessor = require('./commandProcessor')

        log.info(`Connecting to consumer @ hosts ${config.hosts}`)

        consumer = new Kafka.KafkaConsumer(Object.assign({}, default_options, options_consumer))

        consumer.connect({}, (err, res) => {
            if (err) {
                log.error(err)
                return reject(err)
            }
            log.info(`Consumer SSL connection created: ${res.orig_broker_name}`)
        })

        consumer.on("ready", function (arg) {
            consumer.subscribe(config.consumer.topics); //subscribing to all topics
            consumer.consume()
            resolve()

            log.info(`Consumer ${arg.name} subscribed to ${config.consumer.topics}`);
        })

        consumer.on("error.error", function (error) {
            log.info(`Consumer ${arg.name} error ${error}`);
        });

        consumer.on('connection.failure', function (err) {
            log.error('[connection.failure] producer unable to connect ');
            reject(err.message)
        })

        consumer.on('disconnected', function (arg) {
            log.info(`Consumer disconnected: ${JSON.stringify(arg)}`);
        });

        consumer.on("data", function (request) {
            const data = JSON.parse(request.value.toString());
            log.info('Consumer new command: ', data);
            commandProcessor[data.command](data.data || data)
        })
    })
}

/*//////////////////////////////////////////////////////////////*/


/*======== SETTING UP KAFKA PRODUCER FOR EVENT HUB =======*/

let producer;
let options_producer = Object.assign({}, default_options, {
    'dr_cb': true,
    'group.id': `${config.name}_producer`,
    'queue.buffering.max.ms': 0,

    // "debug": "generic,broker,security",
    // 'compression.codec': 'snappy'
})
let topicOpts = {
    'request.required.acks': 1,
    'produce.offset.report': true
};

function connectProducer() {
    return new Promise((resolve, reject) => {

        log.info(`Connecting to producer @ hosts ${config.hosts}`)
        producer = new Kafka.Producer(options_producer, topicOpts);
        producer.setPollInterval(config.producer.pollInterval); //polling for delivery reports
        producer.connect({}, (err, res) => {
            if (err) {
                log.error(err)
                return reject(err)
            }
            log.info(`Producer SSL connection created: ${res.orig_broker_name}`)
        })
        producer.on('ready', function (arg) {
            log.info(`Producer ${arg.name} successfully connected`);
            resolve()
        })
        producer.on('connection.failure', function (err) {
            log.error('[connection.failure] producer unable to connect ');
            reject(err.message)
        })
        producer.on('close', function () {
            log.info('Producer successfully closed');
        })
        producer.on('event.error', function (err) {
            if (err.message != "all broker connections are down") {
                log.error('Error from producer: ', err);
            }
        })
        producer.on('delivery-report', function (err, report) {
            log.debug('delivery-report: ' + JSON.stringify(report));
        });
    })
}

/*//////////////////////////////////////////////////////////////*/

/*==========                    HELPERS               ==========*/

// TODO: HANDLING ERROR
// Method to send messages to the given topic
// message is converted to JSON buffer before they are added in the queue
function send(topic, message) {
    try {

        const msg = JSON.stringify(message)
        log.info(`[${producer.name}] Sending data to topic ${topic}`);
        log.debug(`[${producer.name}] Sending data to topic ${topic} message: ${msg}`);

        // convert objects to JSON strings
        message = Buffer.from(msg);

        //sending message
        producer.produce(topic, -1, message); // -1 for default partition
    } catch (error) {
        log.error(error);
    }
}

// add a listener to the consumer data
function consumer_on_data(cb) {
    consumer.on('data', cb)
}

function isConnected() {
    return producer.isConnected();
}

function closeConnections() {
    if (producer && producer.isConnected()) {
        producer.disconnect();
        log.info(`[PRODUCER] conenction closed`)
    }
    else
        log.info('[Producer] Conenction is already closed')

    if (consumer && consumer.isConnected()) {
        consumer.disconnect();
        log.info(`[CONSUMER] conenction closed`)
    }
    else
        log.info('[Producer] Conenction is already closed')
}

/*//////////////////////////////////////////////////////////////*/

// expose only these methods to the rest of the application and abstract away
module.exports.send = send;
module.exports.isConnected = isConnected;
module.exports.closeConnections = closeConnections;
module.exports.connectConsumer = connectConsumer;
module.exports.consumer_on_data = consumer_on_data;
module.exports.connectProducer = connectProducer;
