const config = require('config')
const rp = require('request-promise')
const bigNumber = require('bignumber.js')


const redis = require('./redis')
const queue = require('./queue')
const logger = require('./logger')
const db = require('../models/db')
const sequelize = require('sequelize')


const log = logger.create('helper');
const { hotWalletCreationConfirmed } = config.kafka.producer.topics
let btc, generateAddress, withdrawals;
const utxosConfirmations = config.confirmations;


module.exports = {

    init: () => {
        return new Promise(async (resolve, reject) => {
            try {
                await redis.connect();
                await db.sync_db()
                await queue.connectProducer()
                await queue.connectConsumer()

                btc = require('../core/BTC_RPC')
                withdrawals = require('../core/withdrawals')
                generateAddress = require('../core/addresses')

                await btc.init()
                await module.exports.createHotWallet('BTC')

                resolve()
            } catch (error) {
                log.error(error)
                await module.exports.closeConnections()
                return reject(error);
            }
        })
    },

    broadcastMessage: (topic, message) => {
        return queue.send(topic, message);
    },

    generateAddress: {
        BTC: () => {
            return new Promise(async (resolve, reject) => {
                generateAddress.BTC().then(resolve).catch(reject);
            })
        }
    },

    withdraw: {
        BTC: (data) => {
            return new Promise(async (resolve, reject) => {
                withdrawals.BTC(data).then(resolve).catch(reject);
            })
        }
    },

    getAddresses: {
        BTC: () => {
            return new Promise(async (resolve, reject) => {
                btc.getAddresses().then(resolve).catch(reject);
            })
        }
    },

    getWalletBalance: {
        BTC: () => {
            return new Promise(async (resolve, reject) => {
                btc.getBalance().then(resolve).catch(reject);
            })
        }
    },

    getUtxos: {
        BTC: () => {
            return new Promise(async (resolve, reject) => {
                btc.getUtxos().then(utxos => {
                    utxos = utxos.filter(utxo => { return utxo.confirmations > 0 });
                    utxos = utxos.sort(function (a, b) {
                        return (b.value - a.value);
                        //sort on height and value
                        // return (a.mintHeight - b.mintHeight && a.value - b.value);
                    });
                    resolve(utxos)
                }).catch(reject);
            })
        }
    },

    newTx: {
        BTC: (params) => {
            return new Promise(async (resolve, reject) => {
                btc.newTx(params).then(resolve).catch(reject);
            })
        }
    },

    signTx: {
        BTC: (params) => {
            return new Promise(async (resolve, reject) => {
                btc.signTx(params).then(resolve).catch(reject);
            })
        }
    },

    broadcastSignedTx: {
        BTC: (signedTx) => {
            return new Promise(async (resolve, reject) => {
                btc.broadcast(signedTx).then(resolve).catch(reject);
            })
        }
    },

    getNetworkFee: {
        BTC: (target = 6) => {
            return new Promise(async (resolve, reject) => {
                btc.getNetworkFee({ target }).then(resolve).catch(reject);
            })
        }
    },

    getBlock: {
        BTC: (hash) => {
            return new Promise(async (resolve, reject) => {
                btc.getBlock(hash).then(resolve).catch(reject);
            })
        }
    },

    hotWallet: (chain) => {
        return new Promise(async (resolve) => {
            let hotwallets = await redis.get('keysAsync', `${chain.toLowerCase()}:address:hot*`);
            return resolve(hotwallets[hotwallets.length - 1].split(':')[3])
        })
    },

    convertToBase: (chain, amount) => {
        switch (chain) {
            case 'BTC':
            case 'btc': {
                return new bigNumber(amount).multipliedBy(1e8)
            }
            default:
                return 0
        }
    },

    convertFromBase: (chain, amount) => {
        switch (chain) {
            case 'BTC':
            case 'btc': {
                return new bigNumber(amount).dividedBy(1e8)
            }
            default:
                return 0
        }
    },

    shiftLeft: async (amount, decimals) => {
        return new bigNumber(amount).shiftedBy(decimals).toFixed(decimals)
    },

    shiftRight: async (amount, decimals) => {
        return new bigNumber(amount).shiftedBy((decimals * -1))
    },

    walletType: async (chain, receiver) => {
        const result = await redis.get('keysAsync', `*${receiver}`);
        if (result.length > 0) {
            const type = result[0].split(':')[2]
            return type
        }
        else
            return 'user'
    },

    // return the cold wallet in ASC order
    coldWalletAddress: (amount, currency) => {
        return new Promise(async (resolve, reject) => {
            try {
                let coldWalletAddress = await db.findOne(
                    db.models().wallet,
                    {
                        type: 'cold',
                        symbol: currency,
                        [sequelize.Op.and]: [
                            sequelize.literal('target_balance-balance = (select max(target_balance-balance) ' +
                                'from wallet where type = "cold" and symbol = ?)')
                        ]
                    },
                    {
                        replacements: [currency],
                        order: [['createdAt', 'DESC']],
                        attributes: ['address', 'target_balance', 'balance', 'createdAt',
                            [sequelize.literal("target_balance-balance"), 'remaining'],
                        ],
                    });
                return resolve(coldWalletAddress.address);
            } catch (error) {
                log.error(error);
                let errorMsg;
                if (error.toString().indexOf('Cannot read property'))
                    errorMsg = `No cold wallet found against ${currency}`
                return reject(errorMsg || error);
            }
        })
    },

    createHotWallet: (symbol) => {
        return new Promise(async (resolve, reject) => {
            try {

                const hotWallet = await db.findOne(
                    db.models().wallet,
                    {
                        symbol: symbol,
                        type: 'hot'
                    },
                    {
                        attributes: ['address']
                    }
                );

                if (!hotWallet) {
                    const address = await module.exports.generateAddress.BTC()

                    await db.create(
                        db.models().wallet,
                        {
                            address: address,
                            symbol: symbol,
                            type: 'hot'
                        })

                    await redis.set('setAsync', `${symbol.toLowerCase()}:address:hot:${address}`, JSON.stringify({}))

                    module.exports.broadcastMessage(hotWalletCreationConfirmed, {
                        symbol: symbol,
                        address: address,
                        type: 'hot_wallet_creation_confirmed',
                    })

                    log.info(`HotWallet: ${address} address created for ${symbol}`)

                    return resolve(address)

                } else
                    resolve(true);

            } catch (error) {
                log.error(error)
                return reject(error)
            }
        })
    },

    httpRequest: (opts) => {
        return new Promise((resolve, reject) => {
            const options = {
                method: opts.method,
                uri: opts.uri,
                body: opts.body || {},
                json: true, // Automatically stringifies the body to JSON,
                timeout: opts.timeout || (20 * 1000),
                headers: opts.headers || {},
                qs: opts.qs || {}
            }
            log.info(`http ${opts.method} request to ${opts.uri}`)
            rp(options)
                .then(function (reponse) {
                    resolve(reponse)
                })
                .catch(function (err) {
                    reject(err)
                })
        })

    },

    closeConnections: () => {
        return new Promise(async (resolve, reject) => {
            redis.quitClient()
            queue.closeConnections()
            db.closeConnection()
            await logger.shutdown()
            if (btc)
                await btc.close()
            setTimeout(resolve, 1000); // resolve after 1 sec
        })
    },
}