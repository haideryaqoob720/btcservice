/*=========================
 * Redis connection handler
 *=========================*/

//Imports
const redis = require('redis')
const bluebird = require('bluebird')
const config = require('config').get('redis')
const log = require('../helpers/logger').create("Redis");


// promisify the redis client
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
let _redis;

function connect() {
    return new Promise((resolve, reject) => {
        // create a new redis client and connect to the redis instance
        _redis = redis.createClient(config.redis_port, config.redis_host, {
            db: config.redis_db
        });

        _redis.on('connect', function () {
            log.info('[REDIS] connected');
            resolve()
        });

        // if an error occurs, print it to the console
        _redis.on('error', function (err) {
            log.error("Error encountered", err.message)
            reject(err.message)
        })

        _redis.on('end', function (err) {
            log.info(`[REDIS] conenction closed`)
        })
    })

}

// function quitClient() {
//     _redis.quit();
// }

module.exports = {
    get: (_function, key) => {
        return new Promise(async (resolve) => {
            if (!_redis.connected) {
                log.error('Redis connected: ', _redis.connected);
                return;
            }
            try {

                const result = await _redis[_function](key);
                return resolve(result);
            } catch (error) {
                log.error(error.message)
            }
        })
    },
    set: (_function, key, value) => {
        return new Promise(async (resolve) => {
            if (!_redis.connected) {
                log.error('Redis connected: ', _redis.connected);
                return;
            }
            try {
                await _redis[_function](key, value);
                return resolve();
            } catch (error) {
                log.error(error.message)
            }
        })
    },

    connect: () => {
        return connect();
    },


    client: () => {
        return _redis;
    },

    quitClient: () => {
        _redis.quit();
    }
}