/**
 * Setup logging system.
 * @param  {Object} [options]
 * @param  {String} [options.logLevel] Minimum logging threshold (default: info).
 * @param  {String} [options.logFolder] Log folder to write logs to.
*/

const log4js = require('log4js');

exports.setup = function (options) {
  const logFolder = options.logFolder;
  const level = options.logLevel || 'info';
  const levelError = 'info';

  const config = {
    appenders: {
      multi: { type: 'multiFile', base: 'logs/', level: 'error', property: 'error', extension: '.log' },
      out: {
        type: 'console',
        layout: {
          type: 'pattern',
          pattern: '%[[%d] [%p] [%X{service}] [%c] %] %m',
        }
      },
    },
    categories: {
      default: { appenders: ['out'], level, enableCallStack: true },
    }
  };

  log4js.configure(config);
};

exports.create = category => {
  const logger = log4js.getLogger(category);
  logger.addContext('service', 'btc')

  // Allow for easy creation of sub-categories.
  logger.create = subCategory => {
    return exports.create(`${category}/${subCategory}`);
  };

  return logger;
};


exports.shutdown = () => {
  return new Promise((resolve, reject) => {
    try {
      log4js.shutdown(resolve);
    } catch (error) {
      reject(error);
    }
  })

}