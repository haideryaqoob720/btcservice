config = {
    log: {
        level: 'info',
        logFolder: './logs/'
    },
    symbol: 'btc',
    confirmations: 6,
    currentBlock: 'latest',
    decimals: 8,
    chains: {
        bitcoin: {
            level_db_wallet_path: '/data/bitcore/wallets',
            network_type: 'mainnet', // mainnet, regtest, testnet,
            baseUrl: 'http://localhost:3300',

            bitcore_host: 'localhost',
            bitcore_port: '3300',

            wallet_name: 'BTC_WALLET_1',
            wallet_seed: 'blush weasel manual such expire into tank eager rural chronic kitten clay'
        }
    },
    redis: {
        redis_port: 6372,
        redis_host: 'localhost',
        redis_db: '1'
    },
    kafka: {
        hosts: ['localhost:7989'],
        name: 'btc_wallet',
        consumer: {
            topics:
                [
                    'btc.wallet.command'
                ]
        },
        producer: {
            pollInterval: 100,
            topics: {
                errors: 'errors',
                coldWallets: 'coldwallets',
                expenses: 'expenses_wallet',
                exHotWallet: 'exchange.hot.wallets',
                commandReply: 'wallet.command.reply',
                transactionConfirmed: 'wallet.transaction.confirmed', // btc.wallet.transaction.confirmed, rlp.wallet.transaction.confirmed
                revenueWithdrawalConfirmed: 'revenue.withdrawal.confirmed',
                hotWalletCreationConfirmed: 'hot.wallet.creation.confirmed'
            }
        },
    },
    database: {
        connection: {
            host: 'localhost',
            port: 3307,
            // user: 'btc-wallet',
            user: 'root',
            password: 'my-secret-pw',
            dbName: 'btc_wallet',
        },
        pool: {
            min: 2,
            max: 10,
        }
    }
}

module.exports = config;