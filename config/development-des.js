config = {
    log: {
        level: 'debug',
        logFolder: './logs/'
    },
    symbol: 'btc',
    confirmations: 1,
    currentBlock: 'latest',
    decimals: 8,
    chains: {
        bitcoin: {
            level_db_wallet_path: '/data/bitcore/wallets',
            network_type: 'testnet', // mainnet, regtest, testnet,
            baseUrl: 'http://5.189.149.113:3000',

            bitcore_host: '5.189.149.113',
            bitcore_port: '3000',

            wallet_name: 'BTC_WALLET_1',
            wallet_seed: 'march fatal fatal weapon renew cool wish path bridge armed detect spider'
        }
    },
    redis: {
        redis_port: 6372,
        redis_host: 'localhost',
        redis_db: '1'
    },
    kafka: {
        hosts: ['localhost:7989'],
        name: 'btc_wallet',
        consumer: {
            topics:
                [
                    'btc.wallet.command'
                ]
        },
        producer: {
            pollInterval: 100,
            topics: {
                errors: 'errors',
                coldWallets: 'coldwallets',
                expenses: 'expenses_wallet',
                exHotWallet: 'exchange.hot.wallets',
                commandReply: 'wallet.command.reply',
                transactionConfirmed: 'wallet.transaction.confirmed', // btc.wallet.transaction.confirmed, rlp.wallet.transaction.confirmed
                revenueWithdrawalConfirmed: 'revenue.withdrawal.confirmed',
                hotWalletCreationConfirmed: 'hot.wallet.creation.confirmed',
            }
        },
    },
    database: {
        connection: {
            host: 'localhost',
            port: 3307,
            user: 'root',
            password: 'my-secret-pw',
            dbName: 'btc_wallet',
        },
        pool: {
            min: 2,
            max: 10,
        }
    }
}

module.exports = config;