FROM node:10.15.3-alpine

RUN apk add --update --no-cache alpine-sdk bash python \
      libressl \
      tar \
      openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev coreutils

WORKDIR /usr/wallet

RUN mkdir -p /usr/wallet/secret

COPY package*.json ./

RUN npm install

COPY . .

CMD ["node", "src/app.js"]